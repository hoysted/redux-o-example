import React, { Component } from 'react';
import './App.css';
import ComponentOne from './containers/componentOne';
import ComponentTwo from './containers/componentTwo';

class App extends Component {
  constructor() {
    super();
  }

  render() {
    //console.log('render');
    return(
      <div className="container">
        index page
        <ComponentOne />
        <ComponentTwo />
      </div>
    );
  }
}

export default App;
