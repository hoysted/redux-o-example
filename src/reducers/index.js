import { SET_VALUE } from './../actions/test';
import { combineReducers } from 'redux';

let initialState = {
};

function testReducer(state = initialState, action) {
    let newState;

    switch(action.type) {
        case SET_VALUE: 
            newState = Object.assign({}, state, {
                [action.data.propertyName]: action.data.value
            });
            return newState;

            // will become the state
        default:
            return state;
    }
}

const reducer = combineReducers({
    test: testReducer
});

export default reducer;