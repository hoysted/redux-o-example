import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setValue } from './../actions/test';

class ComponentOne extends Component {
    componentDidMount() {
        // this is where I am calling the action from - which in turn updates store state
        this.props.dispatchSetValue('First name', 'Luke');
        this.props.dispatchSetValue('Last name', 'Hoysted');
        this.props.dispatchSetValue('First name', 'Olivia');
    }

    render() {
        return(
            <p>component one</p>
        );
    }
}

// Call the 'dispatchSetValue' whatever the hell you want.
// ALL we are doing here is declaring this as a functional prop - NOT CALLING IT
const mapDispatchToProps = (dispatch) => ({
    // prop name : (values passed in) - dispatch does some magic
    dispatchSetValue: (propertyName, value) => dispatch(setValue(propertyName, value))
});

// first param would be map state to props if not used pass null
export default connect('', mapDispatchToProps)(ComponentOne);