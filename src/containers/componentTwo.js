import React, { Component } from 'react';
import { connect } from 'react-redux';

class ComponentTwo extends Component {
    render() {
        console.log();
        const returnedData = Object.keys(this.props.storeData).map((key) => this.props.storeData[key]).join(' ');

        return(
            <React.Fragment>
                <p>{this.props.storeData['First name']} {this.props.storeData['Last name']}</p>
                <p>{returnedData}</p>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    storeData: state.test
});

export default connect(mapStateToProps)(ComponentTwo);