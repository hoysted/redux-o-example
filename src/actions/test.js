export const SET_VALUE = 'SET_VALUE';

export function setValue(propertyName, value) {
    return {
        type: SET_VALUE,
        data: { propertyName, value }
    }
};