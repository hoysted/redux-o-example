import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// just loads the object list not all the actual reducers
import reducer from './reducers/';

// Provider wraps your entire App / component in an overall parent with the store. (basically the store)
import { Provider } from 'react-redux';

//Redux store import
import { createStore } from 'redux';

// I now call this store
const store = createStore(
    reducer,
    //this gives plugin access to the store for dev purposes
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
